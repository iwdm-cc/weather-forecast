package com.example.zc;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import com.example.zc.Bean.CityBean;
import com.example.zc.adapter.CityAdapter;
import com.example.zc.base.BaseActivity;

import java.util.List;

public class CityActivity extends BaseActivity {

    //控件
    private ListView city_lv;
    private ImageButton search_btn;
    private TextView location_tv;
    private List<CityBean> cityBeanList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city);

        setTitle("城市列表");
        //初始化控件
        initView();

        //获取数据
        cityBeanList = dbService.selectMyCity();
        //设置listview显示数据
        setCity_lv(cityBeanList);
        //设置按钮
        setSearch_btn();
        //获取gps定位信息
        getGPS();
    }

    //返回页面处理
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //获取数据
        cityBeanList = dbService.selectMyCity();
        //设置listview显示数据
        setCity_lv(cityBeanList);
    }

    //初始化控件
    private void initView() {
        city_lv = findViewById(R.id.city_lv);
        search_btn = findViewById(R.id.search_ibtn);
        location_tv = findViewById(R.id.location_tv);
    }

    //设置listview显示数据
    private void setCity_lv(List<CityBean> list) {
        //创建适配器
        CityAdapter adapter = new CityAdapter(this, R.layout.city_item, cityBeanList);
        //设置适配器
        city_lv.setAdapter(adapter);
        //设置点击事件
        adapter.setOnItemClickListener(new CityAdapter.OnItemClickListener() {

            @Override
            public void onItemClick(CityBean cityBean) {
                System.out.println("点击了" + cityBean.getCityName());
                //创建意图
                Intent intent = new Intent();
                //放入点击的城市代码
                intent.putExtra("cityid", cityBean.getCityId());
                //设置结果码
                setResult(1, intent);
                //页面返回
                CityActivity.this.finish();
            }

            @Override
            public void onCancelClick(CityBean cityBean) {
                //使用数据库工具类删除点击的城市
                dbService.deleteMyCity(cityBean.getCityId());
                //重新获取数据
                cityBeanList = dbService.selectMyCity();
                //设置listview显示数据
                setCity_lv(cityBeanList);
            }
        });
    }

    //设置按钮
    private void setSearch_btn() {
        search_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CityActivity.this, SearchActivity.class);
                startActivityForResult(intent, 1);
            }
        });
    }

    //获取gps定位信息
    private void getGPS() {
        //获取位置服务
        final LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        //权限检查
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        //设置监听
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 1, new LocationListener() {
            @Override
            public void onLocationChanged(@NonNull Location location) {

            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(@NonNull String provider) {

            }

            @Override
            public void onProviderDisabled(@NonNull String provider) {

            }
        });
        //获取位置
        Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        //构造字符
        if (location!=null){
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("您的位置是：\n");
            stringBuilder.append("经度：");
            stringBuilder.append(location.getLongitude());
            stringBuilder.append("\n纬度：");
            stringBuilder.append(location.getLatitude());
            location_tv.setText(stringBuilder);
        }else {
            location_tv.setText("没有获取到位置信息");
        }
    }
}
