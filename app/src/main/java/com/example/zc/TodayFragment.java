package com.example.zc;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.zc.Bean.WeatherBean;
import com.google.gson.Gson;

import java.util.List;


public class TodayFragment extends Fragment {
    private WeatherBean weatherBean;
    private WeatherBean.CityInfoDTO cityInfoDTO;
    private WeatherBean.DataDTO dataDTO;
    private List<WeatherBean.DataDTO.ForecastDTO> forecastDTOList;
    private String json;
    private TextView ganmao;
    private TextView quantity;
    private TextView sunrise;
    private TextView sunset;
    private TextView shidu;
    private TextView pm25;
    private TextView fx;
    private TextView fl;
    private TextView notice;

    public TodayFragment() {
    }

    public TodayFragment(String json) {
        this.json = json;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_today,null);
        //初始化控件
        initview(view);
        //gson解析json
        gsonObject(json);
        //显示数据到控件
        showdata();

        return view;
    }

    //初始化控件
    private void initview(View view) {
        ganmao = view.findViewById(R.id.ganmao_tv);
        quantity = view.findViewById(R.id.quantity_ty);
        sunrise = view.findViewById(R.id.sunrise_tv);
        sunset = view.findViewById(R.id.sunset_tv);
        shidu = view.findViewById(R.id.shidu_tv);
        pm25 = view.findViewById(R.id.pm25_tv);
        fx = view.findViewById(R.id.fx_tv);
        fl = view.findViewById(R.id.fl_tv);
        notice = view.findViewById(R.id.notice_tv);
    }

    //显示天气数据
    private void showdata() {
        ganmao.setText("今日：" + dataDTO.getGanmao());
        quantity.setText(forecastDTOList.get(0).getAqi() + " - " + dataDTO.getQuality());
        sunrise.setText(forecastDTOList.get(0).getSunrise());
        sunset.setText(forecastDTOList.get(0).getSunset());
        shidu.setText(dataDTO.getShidu());
        pm25.setText(String.valueOf(dataDTO.getPm25()));
        fx.setText(forecastDTOList.get(0).getFx());
        fl.setText(forecastDTOList.get(0).getFl());
        notice.setText("注意：" + forecastDTOList.get(0).getNotice());
    }

    //gson解析
    private void gsonObject(String json) {
        //创建gson对象
        Gson gson = new Gson();
        //gson解析获得weatherBean对象
        weatherBean = gson.fromJson(json, WeatherBean.class);
        //从weatherBean对象中获取cityInfo对象
        cityInfoDTO = weatherBean.getCityInfo();
        //从weatherBean对象中获取data对象
        dataDTO = weatherBean.getData();
        //获取forcast列对象
        forecastDTOList = weatherBean.getData().getForecast();
//        for (WeatherBean.DataDTO.ForecastDTO item : forecastDTOList) {
//            System.out.println(item.toString());
//        }
    }
}
