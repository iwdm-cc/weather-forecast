package com.example.zc.Bean;

public class CityBean {
    private String cityId;
    private String cityName;
    private String citypinyin;

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCitypinyin() {
        return citypinyin;
    }

    public void setCitypinyin(String citypinyin) {
        this.citypinyin = citypinyin;
    }

    @Override
    public String toString() {
        return "CityBean{" +
                "cityId='" + cityId + '\'' +
                ", cityName='" + cityName + '\'' +
                ", citypinyin='" + citypinyin + '\'' +
                '}';
    }
}
