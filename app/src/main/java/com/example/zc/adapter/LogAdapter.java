package com.example.zc.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.zc.R;
import com.example.zc.db.LogBean;

import java.util.List;

public class LogAdapter extends RecyclerView.Adapter<LogAdapter.MyViewHolder> {
    private final Context context;
    private final List<LogBean> forecastDTOList;

    public LogAdapter(@NonNull Context context, @NonNull List<LogBean> objects) {
        this.context = context;
        forecastDTOList = objects;
    }


    @NonNull
    @Override
    public LogAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.log_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LogAdapter.MyViewHolder holder, int position) {
        LogBean detail = forecastDTOList.get(position);
        System.out.println(detail);
        holder.id_tv.setText(detail.getId());
        holder.name_tv.setText(detail.getName());
        holder.time_tv.setText(detail.getDate());
    }

    @Override
    public int getItemCount() {
        return forecastDTOList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView id_tv;
        TextView name_tv;
        TextView time_tv;

        MyViewHolder(View itemView) {
            super(itemView);
            id_tv = itemView.findViewById(R.id.id_tv);
            name_tv = itemView.findViewById(R.id.name_tv);
            time_tv = itemView.findViewById(R.id.time_tv);
        }
    }
}
