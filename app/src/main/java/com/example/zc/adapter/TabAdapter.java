package com.example.zc.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.example.zc.ForecastFragment;
import com.example.zc.TodayFragment;
import com.example.zc.ZxtFragment;


public class TabAdapter extends FragmentStateAdapter {

    private int count;
    private TodayFragment hotFragment;
    private ForecastFragment addFragment;
    private ZxtFragment zxtFragment;

    public void setZxtFragment(ZxtFragment zxtFragment) {
        this.zxtFragment = zxtFragment;
    }

    public TabAdapter(@NonNull FragmentActivity fragmentActivity) {
        super(fragmentActivity);

    }

    public void setHotFragment(TodayFragment hotFragment) {
        this.hotFragment = hotFragment;
    }

    public void setAddFragment(ForecastFragment addFragment) {
        this.addFragment = addFragment;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position) {
            case 0:
                return hotFragment;
            case 1:
                return addFragment;  case 2:
                return zxtFragment;
            default:

                return hotFragment;
        }

    }

    @Override
    public int getItemCount() {
        return count;
    }
}
