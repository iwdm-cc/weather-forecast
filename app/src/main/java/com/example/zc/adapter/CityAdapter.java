package com.example.zc.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.zc.Bean.CityBean;
import com.example.zc.R;

import java.util.List;

public class CityAdapter extends ArrayAdapter<CityBean> {
    private final int newResourceId;

    public CityAdapter(@NonNull Context context, int resource, @NonNull List<CityBean> objects) {
        super(context, resource, objects);
        newResourceId = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = LayoutInflater.from(getContext()).inflate(newResourceId,parent,false);
        CityBean cityBean = getItem(position);
        //找到控件
        TextView TV_name = view.findViewById(R.id.cityname_tv);
        TextView TV_id = view.findViewById(R.id.cityid_tv);
        LinearLayout cancel_ll = view.findViewById(R.id.cancel_ll);
        RelativeLayout city_rl = view.findViewById(R.id.city_rl);
        //设置数据
        TV_name.setText(cityBean.getCityName());
        TV_id.setText(cityBean.getCityId());

        //设置点击监听，点击则执行接口的方法
        cancel_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener!=null){
                    onItemClickListener.onCancelClick(cityBean);
                }
            }
        });
        //设置点击监听，点击则执行接口的方法
        city_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener!=null){
                    onItemClickListener.onItemClick(cityBean);
                }
            }
        });

        return view;
    }

    //创建接口
    private OnItemClickListener onItemClickListener;

    //定义接口
    public interface OnItemClickListener{
        //两个方法
        void onItemClick(CityBean cityBean);
        void onCancelClick(CityBean cityBean);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }
}
