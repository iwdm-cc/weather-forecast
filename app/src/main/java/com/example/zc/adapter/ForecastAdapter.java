package com.example.zc.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.zc.Bean.WeatherBean;
import com.example.zc.R;

import java.util.List;
import java.util.regex.Pattern;

public class ForecastAdapter extends RecyclerView.Adapter<ForecastAdapter.MyViewHolder> {
    private final Context context;
    private final List<WeatherBean.DataDTO.ForecastDTO> forecastDTOList;

    public ForecastAdapter(@NonNull Context context, @NonNull List<WeatherBean.DataDTO.ForecastDTO> objects) {
        this.context = context;

        forecastDTOList = objects;
    }


    @NonNull
    @Override
    public ForecastAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.date_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ForecastAdapter.MyViewHolder holder, int position) {
        WeatherBean.DataDTO.ForecastDTO detail = forecastDTOList.get(position);

        String REGEX = "[^(0-9)]";
        String highString = Pattern.compile(REGEX).matcher(detail.getHigh()).replaceAll("").trim();
        String lowString = Pattern.compile(REGEX).matcher(detail.getLow()).replaceAll("").trim();
        holder.week.setText(detail.getWeek());
        holder.type.setText(detail.getType());
        holder.high.setText(highString);
        holder.low.setText(lowString);
    }

    @Override
    public int getItemCount() {
        return forecastDTOList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView week;
        TextView type;
        TextView high;
        TextView low;

        MyViewHolder(View itemView) {
            super(itemView);
            week = itemView.findViewById(R.id.week_tv);
            type = itemView.findViewById(R.id.type_tv);
            high = itemView.findViewById(R.id.high_tv);
            low = itemView.findViewById(R.id.low_tv);
        }
    }
}
