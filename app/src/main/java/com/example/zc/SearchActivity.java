package com.example.zc;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.example.zc.Bean.CityBean;
import com.example.zc.adapter.CityAdapter;
import com.example.zc.base.BaseActivity;

import java.util.List;

public class SearchActivity extends BaseActivity {

    private ListView city_lv;
    private List<CityBean> cityBeanList;
    private EditText search_et;
    private TextView cancel_tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        setTitle("搜索城市");
        //初始化控件
        initView();
        //获取数据
        cityBeanList = dbService.select();
        //设置listview显示数据
        setCity_lv(cityBeanList);
        //设置取消按钮
        setCancel_tv();
        //设置搜索框
        setSearch_et();
    }

    //初始化控件
    private void initView() {
        //找到listview
        city_lv = findViewById(R.id.city_lv);
        cancel_tv = findViewById(R.id.cancel_tv);
        search_et = findViewById(R.id.search_et);
    }

    //设置搜索框
    private void setSearch_et() {
        search_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                //搜索
                //获取数据
                cityBeanList = dbService.select(s.toString());
                //设置listview显示数据
                setCity_lv(cityBeanList);
            }
        });
    }

    //设置取消按钮
    private void setCancel_tv() {
        cancel_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SearchActivity.this.finish();
            }
        });
    }

    //设置listview显示数据
    private void setCity_lv(List<CityBean> list) {
        //创建适配器
        CityAdapter adapter = new CityAdapter(this, R.layout.city_item, list);
        //设置适配器
        city_lv.setAdapter(adapter);
        //设置监听
        adapter.setOnItemClickListener(new CityAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(CityBean cityBean) {
                System.out.println("点击了" + cityBean.getCityName());
                dbService.saveMyCity(cityBean.getCityId());

                Intent intent = new Intent();
                intent.putExtra("cityid", cityBean.getCityId());
                setResult(1, intent);
                SearchActivity.this.finish();
            }

            @Override
            public void onCancelClick(CityBean cityBean) {

            }
        });

    }
}
