package com.example.zc.init;

import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.TextView;

import com.example.zc.MainActivity;
import com.example.zc.R;
import com.example.zc.base.BaseActivity;


public class WelcomeActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        TextView text = findViewById(R.id.text);

        //创建缩放动画对象
        Animation animation = new ScaleAnimation(0, 1.0f, 0f, 1.0f);
        animation.setDuration(2000);//动画时间
        animation.setRepeatCount(3);//动画的反复次数
        animation.setFillAfter(true);//设置为true，动画转化结束后被应用
        text.startAnimation(animation);//開始动画

        //APP启动后，进入闪屏界面，2秒后
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //登录用户 为空
                if ("".equals(getLoginUser())) {
                    //跳转登录界面
                    navigateTo(LoginActivity.class);
                    finish();
                } else {
                    //已经登录 跳转到主界面
                    navigateTo(MainActivity.class);
                    finish();
                }
            }
        }, 2000);
    }
}