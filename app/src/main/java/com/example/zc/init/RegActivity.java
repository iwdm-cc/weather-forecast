package com.example.zc.init;

import static com.example.zc.util.Util.SLAT;

import android.content.ContentValues;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.zc.R;
import com.example.zc.base.BaseActivity;
import com.example.zc.util.Util;

public class RegActivity extends BaseActivity {
    private TextView input1;
    private TextView input2;
    private TextView input3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reg);
        input1 = findViewById(R.id.input_1);
        input2 = findViewById(R.id.input_2);
        input3 = findViewById(R.id.input_3);

    }

    //注册
    public void regClick(View view) {
        String name = input1.getText().toString();
        String pwd = input2.getText().toString();
        String r_pwd = input3.getText().toString();


        if (TextUtils.isEmpty(name)) {
            Toast.makeText(RegActivity.this, "请输入账号~", Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(pwd)) {
            Toast.makeText(RegActivity.this, "请输入密码~", Toast.LENGTH_SHORT).show();
            return;
        }
        if (!r_pwd.equals(pwd)) {
            Toast.makeText(RegActivity.this, "两次密码不匹配~", Toast.LENGTH_SHORT).show();
            return;
        }


        ContentValues value = new ContentValues();

        String md5Str = Util.md5(name + "" + pwd, SLAT);
        value.put("username", name);
        value.put("password", md5Str);
        try {
            dbService.insertPerson(value);
            Toast.makeText(RegActivity.this, "注册成功，请登录~", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(RegActivity.this, "账号已经存在，请登录~", Toast.LENGTH_SHORT).show();
        }
        finish();
    }
}