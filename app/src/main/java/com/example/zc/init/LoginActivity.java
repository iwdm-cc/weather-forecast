package com.example.zc.init;

import static com.example.zc.util.Util.SLAT;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.example.zc.MainActivity;
import com.example.zc.R;
import com.example.zc.base.BaseActivity;
import com.example.zc.db.LogService;
import com.example.zc.util.Util;

public class LoginActivity extends BaseActivity {

    private TextView input1;
    private TextView input2;
    private CheckBox auto;
    private LogService logService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        logService = new LogService(this);

        input1 = findViewById(R.id.input_1);
        input2 = findViewById(R.id.input_2);
        auto = findViewById(R.id.auto);

    }

    //登录
    public void loginClick(View view) {

        String name = input1.getText().toString();
        String pwd = input2.getText().toString();

        if (TextUtils.isEmpty(name)) {
            Toast.makeText(LoginActivity.this, "请输入账号~", Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(pwd)) {
            Toast.makeText(LoginActivity.this, "请输入密码~", Toast.LENGTH_SHORT).show();
            return;
        }

        String login = dbService.login(name);
        String md5Str = Util.md5(name + "" + pwd, SLAT);
        if (md5Str.equals(login)) {
            Toast.makeText(LoginActivity.this, "登录成功~", Toast.LENGTH_SHORT).show();
            if (auto.isChecked()) {
                setLoginUser(name);
            }
            logService.addLog(name);

            startActivity(new Intent(LoginActivity.this, MainActivity.class).putExtra("username", name));
            finish();
        } else {
            Toast.makeText(LoginActivity.this, "密码错误~", Toast.LENGTH_SHORT).show();
        }
    }

    //注册
    public void regClick(View view) {
        startActivity(new Intent(LoginActivity.this, RegActivity.class));
    }


}