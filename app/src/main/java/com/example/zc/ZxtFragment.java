package com.example.zc;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.zc.Bean.WeatherBean;

import java.util.ArrayList;
import java.util.List;


public class ZxtFragment extends Fragment {
    private WeatherBean weatherBean;
    private WeatherBean.CityInfoDTO cityInfoDTO;
    private WeatherBean.DataDTO dataDTO;
    private List<WeatherBean.DataDTO.ForecastDTO> forecastDTOList;
    private RecyclerView forecasr_lv;
    private Context context;
    private WeatherTrendGraph mWeatherTrendGraph;
    private List<Weather> mWeathers = new ArrayList<>();
    private Weather weather;
    public ZxtFragment(String json) {
    }

    public ZxtFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_zxt,null);
        context = view.getContext();

        mWeatherTrendGraph = (WeatherTrendGraph) view.findViewById(R.id.weatherTrendGraph);
        weather = new Weather(28, 16, "2017-04-18", "晴");
        mWeathers.add(weather);
        weather = new Weather(30, 17, "2017-04-19", "晴");
        mWeathers.add(weather);
        weather = new Weather(23, 13, "2017-04-20", "阴");
        mWeathers.add(weather);
        weather = new Weather(20, 13, "2017-04-21", "多云");
        mWeathers.add(weather);
        weather = new Weather(25, 17, "2017-04-22", "雨");
        mWeathers.add(weather);
        weather = new Weather(27, 17, "2017-04-23", "晴");
        mWeathers.add(weather);
        weather = new Weather(22, 16, "2017-04-24", "阴");
        mWeathers.add(weather);
        weather = new Weather(20, 14, "2017-04-25", "晴");
        mWeathers.add(weather);
        mWeatherTrendGraph.setWeathers(mWeathers);
        return view;
    }



}
