package com.example.zc;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.zc.adapter.LogAdapter;
import com.example.zc.base.BaseActivity;
import com.example.zc.db.LogBean;
import com.example.zc.db.LogService;

import java.util.List;

public class LogActivity extends BaseActivity {
    private LogService logService;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log);

        logService = new LogService(this);

        RecyclerView recyclerView = findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        List<LogBean> data = logService.selectLog(null);
        recyclerView.setAdapter(new LogAdapter(this,data));
    }
}