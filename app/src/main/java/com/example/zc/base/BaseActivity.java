package com.example.zc.base;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.zc.db.MyDbService;


public abstract class BaseActivity extends AppCompatActivity {

    private SharedPreferences sharedPreferences;
    public Context context;
    public MyDbService dbService;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences = getSharedPreferences("zc", Context.MODE_PRIVATE);
        context = this;
        dbService = new MyDbService(this);
    }
    public void navigateToWithFlag(Class cls, String flags) {
        Intent in = new Intent(context, cls);
        in.putExtra("flags", flags);
        startActivity(in);
    }
    public void showToast(String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    //跳转界面
    public void navigateTo(Class cls) {
        Intent in = new Intent(context, cls);
        startActivity(in);
    }

    //记住用户登录状态
    public void setLoginUser(String loginUser) {

        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString("loginUser", loginUser);
        edit.apply();
    }

    //获取登录用户
    public String getLoginUser() {
        return sharedPreferences.getString("loginUser", "");
    }

    //退出登录
    public void signOut() {
        //退出 即 登录用户为空
        setLoginUser("");
    }

}
