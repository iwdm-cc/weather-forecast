package com.example.zc;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.viewpager2.widget.ViewPager2;

import com.example.zc.Bean.WeatherBean;
import com.example.zc.adapter.TabAdapter;
import com.example.zc.base.BaseActivity;
import com.example.zc.util.HttpUtil;
import com.google.gson.Gson;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainActivity extends BaseActivity implements View.OnClickListener {
    //定义对象
    private WeatherBean weatherBean;
    private WeatherBean.CityInfoDTO cityInfoDTO;
    private WeatherBean.DataDTO dataDTO;
    private List<WeatherBean.DataDTO.ForecastDTO> forecastDTOList;
    private static final String httpString = "http://t.weather.itboy.net/api/weather/city/";
    //
    private String cityId = "101040100";
    private String json;
    private AnimationDrawable animation;
    //定义控件
    private ImageView background_iv;
    private TextView time;
    private TextView city;
    private TextView type;
    private TextView wendu;
    private TextView high;
    private TextView low;

    //创建线程池
    private ExecutorService threadPool = Executors.newSingleThreadExecutor();

    //创建map缓存信息，线程安全，解决java.Util.ConcurrentModificationException并发修改异常
    private Map<String, JsonAndTime> jsonMap = new ConcurrentHashMap<>();
    private ViewPager2 viewPager;
    //线程不安全
//    private Map<String, JsonAndTime> jsonMap = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //加载界面布局
        setContentView(R.layout.activity_main);
        //初始化控件
        initView();

        //获取json信息
        getJson(cityId);
    }

    // 主线程创建消息处理器
    private Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            //200则成功
            if (msg.what == 200) {
                //接收线程获取的资源
                json = (String) msg.obj;
                //gson解析获取对象
                gsonObject();
                //显示天气数据
                showData();
                //执行今日天气点击

                //0则失败
            } else if (msg.what == 0) {
                Toast.makeText(MainActivity.this, "请检查网络", Toast.LENGTH_SHORT).show();
            }
        }
    };

    //初始化控件
    private void initView() {
        background_iv = findViewById(R.id.background_iv);
        time = findViewById(R.id.time_tv);
        city = findViewById(R.id.city_tv);
        type = findViewById(R.id.type_today_tv);
        wendu = findViewById(R.id.wendu_tv);
        high = findViewById(R.id.high_today_tv);
        low = findViewById(R.id.low_today_tv);
        viewPager = findViewById(R.id.viewPager);

    }

    //获取json信息
    private void getJson(String cityId) {
        //创建消息
        Message message = new Message();
        //判断是否有缓存
        if (jsonMap.containsKey(cityId)) {
            //判断时间戳是否相差30000ms即30s
            if (System.currentTimeMillis() - jsonMap.get(cityId).getCurrentTime() <= 30000) {
                //如果30s内有获取过，则取出使用
                String myJson = jsonMap.get(cityId).getJson();
                //设置消息类型
                message.what = 200;
                //设置传送的数据
                message.obj = myJson;
                //发送消息到消息队里进行下一步操作
                handler.sendMessage(message);
                //返回，结束方法
                return;
            }
        }
        //处理得到url
        String urlString = httpString + cityId;
        //通过线程池开启线程获取天气信息
        threadPool.execute(() -> {
            //通过网络资源获取json数据
            String myJson = HttpUtil.getJsonInfo(urlString);
            //如果资源获取成功
            if (myJson != null) {
                System.out.println("请求成功");
                //存入map中
                jsonMap.put(cityId, new JsonAndTime(System.currentTimeMillis(), myJson));
                System.out.println(jsonMap);
                System.out.println(jsonMap.get(cityId).toString());
                //调用自身再次尝试获取json数据
                getJson(cityId);
            } else {
                System.out.println("请求失败");
                //设置消息类型
                message.what = 0;
                //设置传送的数据
                message.obj = null;
                //发送消息到消息队里进行下一步操作
                handler.sendMessage(message);
            }
        });
    }

    //gson解析
    private void gsonObject() {
        //创建gson对象
        Gson gson = new Gson();
        //gson解析获得weatherBean对象
        weatherBean = gson.fromJson(json, WeatherBean.class);
        //从weatherBean对象中获取cityInfo对象
        cityInfoDTO = weatherBean.getCityInfo();
        //从weatherBean对象中获取data对象
        dataDTO = weatherBean.getData();
        //获取forcast列对象
        forecastDTOList = weatherBean.getData().getForecast();


    }

    //显示天气数据
    private void showData() {
        time.setText(weatherBean.getTime());
        city.setText(cityInfoDTO.getCity());
        type.setText(forecastDTOList.get(0).getType());
        wendu.setText(dataDTO.getWendu());
        high.setText(forecastDTOList.get(0).getHigh());
        low.setText(forecastDTOList.get(0).getLow());
        //设置背景
        setBackground();
        TabAdapter tabAdapter = new TabAdapter(MainActivity.this);
        tabAdapter.setHotFragment(new TodayFragment(json));
        tabAdapter.setAddFragment(new ForecastFragment(json));
        tabAdapter.setZxtFragment(new ZxtFragment(json));
        tabAdapter.setCount(3);
        viewPager.setOffscreenPageLimit(3);
        viewPager.setAdapter(tabAdapter);

    }

    //设置天气背景
    private void setBackground() {
        background_iv.setBackgroundResource(R.drawable.duoyun_day);
        if (animation != null) {
            animation.stop();
            animation = null;
        }
//        forecastDTOList.get(0).setType("雷阵雨");
        switch (forecastDTOList.get(0).getType()) {
            case "暴雨":
                background_iv.setBackgroundResource(R.drawable.baoyu_frame);
                animation = (AnimationDrawable) background_iv.getBackground();
                animation.start();
                break;
            case "雷阵雨":
                background_iv.setBackgroundResource(R.drawable.baoyu_frame);
                animation = (AnimationDrawable) background_iv.getBackground();
                animation.start();
                break;
            case "大雨":
                background_iv.setBackgroundResource(R.drawable.dayu_frame);
                animation = (AnimationDrawable) background_iv.getBackground();
                animation.start();
                break;
            case "中雨":
                background_iv.setBackgroundResource(R.drawable.dayu_frame);
                animation = (AnimationDrawable) background_iv.getBackground();
                animation.start();
                break;
            case "小雨":
                background_iv.setBackgroundResource(R.drawable.xiaoyu_frame);
                animation = (AnimationDrawable) background_iv.getBackground();
                animation.start();
                break;
            case "阵雨":
                background_iv.setBackgroundResource(R.drawable.xiaoyu_frame);
                animation = (AnimationDrawable) background_iv.getBackground();
                animation.start();
                break;
            case "多云":
                background_iv.setBackgroundResource(R.drawable.duoyun_day);
                break;
            case "阴":
                background_iv.setBackgroundResource(R.drawable.yin_day);
                break;
            case "晴":
                background_iv.setBackgroundResource(R.drawable.duoyun_day);
                break;
        }
    }

    //创建OptionMenu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //加载菜单资源
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    //OptionMenu菜单项被选中时的方法
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                signOut();
                finish();
                break;
            case R.id.city:
                //创建意图
                Intent intent = new Intent(MainActivity.this, CityActivity.class);
                //打开新页面
                startActivityForResult(intent, 1);
                break;
            case R.id.log:
              navigateTo(LogActivity.class);
                break;
            default:
                break;

        }
        return true;
    }

    //按钮点击事件，显示不同的fragment
    @Override
    public void onClick(View v) {
        //更多按钮的点击事件
        if (v.getId() == R.id.city) {
            //创建意图
            Intent intent = new Intent(MainActivity.this, CityActivity.class);
            //打开新页面
            startActivityForResult(intent, 1);
            //其他按钮的点击事件，如果有数据，才进行
        } else {
            //否则提示没有网络
            Toast.makeText(MainActivity.this, "请检查网络", Toast.LENGTH_SHORT).show();
            //尝试获取json数据
            getJson(cityId);
        }
    }

    //页面返回后处理信息
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            if (resultCode == 1) {
                if (requestCode == 1) {
                    try {
                        //取出返回的城市id
                        cityId = data.getStringExtra("cityid");
                        //获取json数据
                        getJson(cityId);
                    } catch (Exception e) {
                        System.out.println("没数据");
                    }
                }
            }
        }
    }

    //辅助存放实体类
    static class JsonAndTime {
        private Long currentTime;
        private String json;

        public JsonAndTime(Long currentTime, String json) {
            this.currentTime = currentTime;
            this.json = json;
        }

        public Long getCurrentTime() {
            return currentTime;
        }

        public void setCurrentTime(Long currentTime) {
            this.currentTime = currentTime;
        }

        public String getJson() {
            return json;
        }

        public void setJson(String json) {
            this.json = json;
        }

        @Override
        public String toString() {
            return "JsonAndTime{" +
                    "currentTime=" + currentTime +
                    ", json='" + json + '\'' +
                    '}';
        }
    }
}