package com.example.zc.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

public class LogService extends MyDbService {
    public LogService(Context context) {
        super(context);
    }

    public void addLog(ContentValues data) {
        getDb().insert("login_record", null, data);
    }

    public List<LogBean> selectLog(ContentValues data) {
        List<LogBean> list = new ArrayList<>();
        Cursor cursor = getDb().rawQuery("select login_record.*,user.username from login_record left join user on login_record.uid = user._id", null, null);
        if (cursor.moveToFirst()) {
            do {
                LogBean log =new LogBean();
                log.setId(cursor.getString(cursor.getColumnIndex("_id")));
                log.setUid(cursor.getString(cursor.getColumnIndex("uid")));
                log.setDate(cursor.getString(cursor.getColumnIndex("logtime")));
                log.setName(cursor.getString(cursor.getColumnIndex("username")));
                list.add(log);
            }while (cursor.moveToNext());
        }
        System.out.println("login_record:"+cursor.getCount());
        cursor.close();
        return list;
    }

    public void addLog(String name) {
        ContentValues data = new ContentValues();
        Cursor cursor = getDb().rawQuery("select * from user where username = ?", new String[]{name});
        cursor.moveToFirst();
        String uid = cursor.getString(cursor.getColumnIndex("_id"));
        cursor.close();
        data.put("uid", uid);
        getDb().insert("login_record", null, data);
    }
}
