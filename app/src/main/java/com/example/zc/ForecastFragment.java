package com.example.zc;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.zc.Bean.WeatherBean;
import com.example.zc.adapter.ForecastAdapter;
import com.google.gson.Gson;

import java.util.List;


public class ForecastFragment extends Fragment {
    private WeatherBean weatherBean;
    private WeatherBean.CityInfoDTO cityInfoDTO;
    private WeatherBean.DataDTO dataDTO;
    private List<WeatherBean.DataDTO.ForecastDTO> forecastDTOList;
    private String json;
    private RecyclerView forecasr_lv;
    private Context context;

    public ForecastFragment(String json) {
        this.json = json;
    }

    public ForecastFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_forecast,null);
        context = view.getContext();
        //gson解析json
        gsonObject(json);

        //初始化控件并显示
        initView(view);

        return view;
    }
    //初始化控件
    private void initView(View view){
        forecasr_lv = view.findViewById(R.id.forecast_lv);
        forecasr_lv.setLayoutManager(new LinearLayoutManager(context));
        forecasr_lv.setAdapter(new ForecastAdapter(context,forecastDTOList));
    }

    //gson解析
    private void gsonObject(String json) {
        //创建gson对象
        Gson gson = new Gson();
        //gson解析获得weatherBean对象
        weatherBean = gson.fromJson(json, WeatherBean.class);
        //从weatherBean对象中获取cityInfo对象
        cityInfoDTO = weatherBean.getCityInfo();
        //从weatherBean对象中获取data对象
        dataDTO = weatherBean.getData();
        //获取forcast列对象
        forecastDTOList = weatherBean.getData().getForecast();
    }
}
